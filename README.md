# OACISのdockerイメージに開発環境インストール
- メンテナンサー：内種岳詞<takeshi.uchitane@riken.jp>

# 基本情報
- OACIS:国立研究開発法人理化学研究所計算科学研究機構離散事象シミュレーション研究チームによって開発されたシミュレーション実行・管理ソフト
    - https://github.com/crest-cassia/oacis
- OACISをdockr仮想環境で実行する準備が開発者らによって進められている
    - https://github.com/crest-cassia/oacis_docker (現在非公開)
    - イメージは取得可能
        - https://registry.hub.docker.com/u/takeshiuchitane/oacis/
    - dockerについては以下のリンクを参照
        - https://www.docker.com/
    - dockerエンジンのインストール方法
        - (windows) https://docs.docker.com/installation/windows/
        - (Mac OS X) https://docs.docker.com/installation/mac/
        - (ubuntu) https://docs.docker.com/installation/ubuntulinux/

# 追加インストールされる開発環境
- R
    - 追加ライブラリとしてpsychをインストール
- vim
    - テキストファイルの編集用

# 利用方法
0. dockerエンジンのインストール，インターネット接続,gitのインストール
1. oacis_dev_doclerのダウンロード

        cd ~/path/to/download
        git clone https://t-uchitane@bitbucket.org/t-uchitane/oacis_dev_docker.git

2. runスクリプトの実行

        cd ~/work_dir
        /path/to/oacis_dev_docker/bin/run_oacis_docker.sh PROJECT_NAME PORT
        #~/download/oacis_dev_docker/bin/run_oacis_docker.sh TrafficSim 3000
    - このとき，oacisのdockerイメージのダウンロードと開発環境の追加インストールが行われる