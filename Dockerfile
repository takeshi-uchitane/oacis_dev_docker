FROM oacis/oacis:develop
MAINTAINER "Takeshi Uchitane" <t.uchitane@gmail.com>

ENV HOME /root
#Setup packages to build and analyze
RUN echo "deb http://cran.rstudio.com/bin/linux/ubuntu trusty/" >> /etc/apt/sources.list;gpg --keyserver keyserver.ubuntu.com --recv-key 51716619E084DAB9; gpg -a --export 51716619E084DAB9 | sudo apt-key add - ; apt-get update && apt-get install -y r-base gnuplot vim python-matplotlib unzip wget nodejs npm cmake; apt-get clean; npm install -g json; ln -s /usr/bin/nodejs /usr/local/bin/node
RUN /bin/cp -f /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

USER oacis
ENV HOME /home/oacis
WORKDIR /home/oacis
ADD .vimrc /home/oacis/
RUN git clone https://github.com/gmarik/Vundle.vim.git /home/oacis/.vim/bundle/Vundle.vim; vim +PluginInstall +qall
ADD install-packages.R /home/oacis/
RUN Rscript install-packages.R; rm install-packages.R*; echo "export R_HOME=/usr/lib/R" >> /home/oacis/.bashrc

USER root
#Expose ports
EXPOSE 3000
#Create data volumes for OAICS
VOLUME ["/data/db"]
VOLUME ["/home/oacis/oacis/public/Result_development"]

CMD ["./oacis_docker_cmd/oacis_start.sh"]
